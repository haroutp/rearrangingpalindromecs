﻿using System;
using System.Collections.Generic;

namespace PalindromeRearranging
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        bool palindromeRearranging(string inputString) {
            Dictionary<char, int> dict = new Dictionary<char, int>();
            foreach(char item in inputString){
                if(!dict.ContainsKey(item)){
                    dict[item] = 1;
                }else{
                    dict[item] += 1;
                }
            }
            
            List<int> listOfOdds = new List<int>();
            foreach (var item in dict.Values)
            {
                if(item % 2 != 0){
                    listOfOdds.Add(item);
                }
            }
            
            if(listOfOdds.Count > 1){
                return false;
            }
            return true;
        }

    }
}
